from github import Github
from git import Repo
from time import sleep
import os
import threading
import logging
import json
from config import TOKEN, N_PROJECTS, PATH
import sys

class RepositoryInfo(object):
    def __init__(self, repo_id, created_at, updated_at, folder_name, default_branch, branches_url, owner, name, clone_url, size, stargazers_count, watchers_count, forks_count, search_query, fork):
        self.repo_id = repo_id
        self.created_at = str(created_at)
        self.updated_at = str(updated_at)
        self.default_branch = default_branch
        self.branches_url = branches_url
        self.owner = owner
        self.name = name
        self.stargazers_count = stargazers_count
        self.watchers_count = watchers_count
        self.clone_url = clone_url
        self.size = size    
        self.search_query = search_query
        self.forks_count = forks_count
        self.folder_name = folder_name
        self.fork = fork

def obj_dict(obj):
    return obj.__dict__

logging.basicConfig(filename = 'logfile.log', filemode='w', format='%(asctime)s - %(levelname)s: %(message)s', level=logging.INFO)

g = Github(TOKEN)

MAX_THREADS = 15
N_THREADS = 10

def main():
    querys_repository = []
    
    keywords = ['machine learning',
                'classification',
                'regression',
                'gradient descent algorithm',
                'clustering',
                'association',
                'decision trees',
                'support vector machines',
                'neural networks',
                'deep learning',
                'reinforcement learning',
                'bayesian',
                'cross validation',
                'robotics',
                'computational neural networks',
                'natural language processing',
                'database',
                'computer vision',
                'supervised learning',
                'unsupervised learning',
                'reinforcement learning',
                'overfitting',
                'text classification',
                'text sorting',
                'sentiment analysis',
                'information extraction',
                'named-entity recognition',
                'speech recognition',
                'natural language understanding',
                'natural language generation',
                'machine translation',
                'train dataset',
                'validate dataset',
                'test dataset',
                'image classification',
                'target detection',
                'image segmentation',
                'significance test'
                ]
    
    for keyword in keywords:
        query = 'language:python ' + keyword
        print(query)
        querys_repository.append(query)
    
    
    
    # querys_repository.append('language:python machine learning stars:<500')
    # querys_repository.append('language:python topic:machine-learning')
    # querys_repository.append('language:python topic:deep-learning')
    # querys_repository.append('language:python topic:neural-network')
    # querys_repository.append('language:python topic:neural-network topic:machine-learning topic:deep-learning')
    # querys_repository.append('language:python topic:tensorflow')
    # querys_repository.append('language:python topic:sklearn')
    
    projects_to_clone = []
    
    urls = set()
    
    map_repeats = {}
    
    for query in querys_repository:
        repositories = g.search_repositories(query, 'stars', 'desc');
            
        logging.info('search: %s', query)  
    
        count = 0
    
        for index, repo in enumerate(repositories):
            if index > N_PROJECTS - 1:
                break
            
            url = repo.clone_url
            
            have = url in urls
            
            if (have):
                count = count + 1
            else:
                logging.info("%d %s %s %s %d %s", repo.id, repo.owner.login, repo.name, repo.clone_url, repo.size, repo.fork)
                
                folder_name = str(repo.id) + '_' + repo.owner.login + '-' + repo.name
                
                info = RepositoryInfo(repo.id, 
                                      repo.created_at, 
                                      repo.updated_at,
                                      folder_name,
                                      repo.default_branch, 
                                      repo.branches_url, 
                                      repo.owner.login, 
                                      repo.name, 
                                      repo.clone_url, 
                                      repo.size, 
                                      repo.stargazers_count,
                                      repo.watchers_count,
                                      repo.forks_count,
                                      query,
                                      repo.fork
                                      )
                
                
                
                projects_to_clone.append(info)
                
                urls.add(repo.clone_url)
            
            sleep(0.3)
            
        sleep(0.4)
            
        map_repeats[query] = count
    
    
    total_to_clone = int(len(urls))
    
    logging.info('Projects to clone: %d', total_to_clone)
    
    logging.info('saving infos...')
    
    with open(PATH + 'input/querys.json', 'w') as file:
        file.write(json.dumps(querys_repository))
    
    with open(PATH + 'input/urls.json', 'w') as file:
        file.write(json.dumps(list(urls)))
            
    with open(PATH + 'input/repeats.json', 'w') as file:
        file.write(json.dumps(map_repeats))
        
    with open(PATH + 'input/repos.json', 'w') as file:
        file.write(json.dumps(projects_to_clone, default = obj_dict))
        
    
    logging.info('download...')
      
    def worker(clone_url, folder):
        global N_THREADS
        N_THREADS += 1
        Repo.clone_from(clone_url, os.path.join(PATH + 'input', folder))
        N_THREADS -= 1
    #     print('done %s' % project[0])
      
    while len(projects_to_clone) > 0:
        if N_THREADS < MAX_THREADS:
            project = projects_to_clone.pop()
            logging.info('cloning %s', project.folder_name)
            
            if not os.path.exists(PATH + 'input/' + project.folder_name):        
                threading.Thread(target = worker, args = [project.clone_url, project.folder_name]).start()
            else:
                logging.info('%s exists', project.folder_name)      
          
    logging.info("done")    
          
if __name__ == "__main__":
    main()
     
    
    