# Debugging Machine Learning: Repository Tool

## Configurações

Editar as configurações no arquivo config.py.

```python
token = ' ACCESS TOKEN '
path = '/home/moabson/ml-debugging-find-tool/'
```

Onde,

* **token**: seu token de acesso;
* **path:** diretório contendo um outro diretório dentro chamado *input*.

## Executar

```bash
python3.7 main.py
```

## Acompanhar o log

```bash
tail -f -n 50 logfile.log
```

## Resultado
Ao final do processo todos os repositórios estarão dentro do diretório *input* no local definido, além disso serão criados quatro arquivos no formato JSON, sendo eles:
* **querys.json:** as querys utilizadas;
* **urls.json:** as urls de todos os projetos selecionados;
* **repeats.json:** número de projetos que já foram selecionados por query;
* **repos.json:** informações dos repositórios baixados.

Obs.: os projetos são selecionados por ordem descrescente de estrelas e que não são *forks*, cada *query* é limitada a 100 repositórios.


As querys são: 

```
language:python machine learning
language:python data mining
language:python data science
language:python clustering
language:python classification
language:python supervised learning
language:python adaboost
language:python bayesian regression
language:python decision tree
language:python knn
language:python linear regression
language:python logistic regression
language:python naive bayes
language:python perceptron
language:python random forest
language:python xgboost
language:python unsupervised learning
language:python hierarchical clustering
language:python hca
language:python dbscan
language:python optics
language:python genetic algorithm
language:python k-means
language:python reinforcement learning
language:python deep q-network
language:python deep learning
language:python neural network
language:python convolutional neural network
language:python cnn
language:python multilayer perceptron
language:python recurrent neural network
```