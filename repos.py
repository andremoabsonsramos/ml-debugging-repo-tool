import json

querys = {}

with open("repos.json", "r") as file:
    data = json.load(file)
    
    for value in data:
        
        query = value["search_query"]
        have_query = query in querys.keys()
        
        if not have_query:
            querys[query] = 1
        else:
            hits = querys[query]
            querys[query] = hits + 1
            
    print(querys)