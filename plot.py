import matplotlib.pyplot as plot
import json

with open('result.json', 'r') as file:
    data = json.load(file)

#     for value in data:
#         print(value['projects'])
     
    graph = [(v['keyword'], v['countProjects']) for v in data]

    graph.sort(key = lambda x : x[1], reverse = True)

    x = list(zip(*graph))[0]
    y = list(zip(*graph))[1]
    
    plot.bar(x, y)
    plot.xlabel("tools")
    plot.ylabel("number of projects")
    plot.show()